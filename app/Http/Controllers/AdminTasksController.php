<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;

class AdminTasksController extends Controller
{
    public function index()
    {
        //$tasks = auth()->user()->tasks();
		$tasks = Task::all();
        return view('admin.tasks', compact('tasks'));
    }
    
    public function add()
    {
    	return view('admin.add');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'description' => 'required'
        ]);
    	$task = new Task();
    	$task->description = $request->description;
    	$task->user_id = auth()->user()->id;
    	$task->save();
    	return redirect('/admin'); 
    }

    public function edit(Task $task)
    {
    	//if (auth()->user()->id == $task->user_id)
        //{            
                return view('admin.edit', compact('task'));
        //}           
        //else {
        //     return redirect('/admin/tasks');
        //}            	
    }

    public function update(Request $request, Task $task)
    {
    	if(isset($_POST['delete'])) {
    		$task->delete();
    		return redirect('/admin/tasks');
    	}
    	else
    	{
            $this->validate($request, [
                'description' => 'required'
            ]);
    		$task->description = $request->description;
	    	$task->save();
	    	return redirect('/admin/tasks'); 
    	}    	
    }
}
