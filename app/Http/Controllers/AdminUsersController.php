<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AdminUsersController extends Controller
{
    public function index()   
    {
		$users = User::all();
        return view('admin.users', compact('users'));
    }
}
