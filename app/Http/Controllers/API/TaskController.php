<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use App\Models\Task;
use App\Http\Resources\Task as TaskResource;
   
class TaskController extends BaseController
{
    public function index()
    {
        $tasks = Task::where('user_id', auth()->user()->id)
               ->get();
        return $this->sendResponse(TaskResource::collection($tasks), 'Tasks fetched.');
    }
    
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'description' => 'required'
        ]);
        
        if ($validator->fails()){
            return $this->sendError($validator->errors());       
        }
        
        $task = new Task();
        $task->description = $request->description;
        $task->user_id = auth()->user()->id;
        $task->save();
        
        return $this->sendResponse(new TaskResource($task), 'Task created.');
    }
   
    public function show($id)
    {
        $task = Task::find($id);
        
        if (is_null($task)) {
            return $this->sendError('Task does not exist.');
        }
        // comprobar que la tarea pertence al usuario
        // si es del usuario, se muestra
        // si no es del usuario, devolver error
        if (auth()->user()->id == $task->user_id)
            return $this->sendResponse(new TaskResource($task), 'Task fetched.');
        else 
            return $this->sendError("Error: tarea inaccesible");
    }
    
    public function update(Request $request, Task $task)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'description' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError($validator->errors());       
        }

        // comprobar que la tarea pertence al usuario
        // si es del usuario, se actualiza
        // si no es del usuario, devolver error
        if (auth()->user()->id == $task->user_id) {
            $task->description = $input['description'];
            //$task->description = $request->description;
            $task->save();
            
            return $this->sendResponse(new TaskResource($task), 'Task updated.');
        } else {
            return $this->sendError("Error: tarea inaccesible");
        }                
    }
   
    public function destroy($id)
    {
        $task = Task::find($id);
        if (is_null($task)) {
            return $this->sendError('Task does not exist.', 'Task NOT deleted.');
        }
        else {
        // comprobar que la tarea pertence al usuario
        // si es del usuario, se borra
        // si no es del usuario, devolver error
            if (auth()->user()->id == $task->user_id) {
                $task->delete();
                
                return $this->sendResponse([], 'Task deleted.');
            } else {
                return $this->sendError("Error: tarea inaccesible");      
            }          
        }
    }
}
