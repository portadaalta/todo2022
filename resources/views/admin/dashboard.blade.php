<x-app-layout>
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Dashboard') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">
            <div class="flex">
                <div class="flex-auto text-2xl mb-4 text-green-500">Dashboard Administration</div>
                
            </div>
            
            <div class="bg-gray-200 p-6 grid grid-rows-2">
				<div class="m-2 p-6">
					<a href="{{ url('/admin/tasks') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Administrar tareas</a>
				</div>
				<div class="m-2 p-6">
					<a href="{{ url('/admin/users') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Ver usuarios</a>
				</div>
            </div>
        </div>
    </div>
</div>
</x-app-layout>

