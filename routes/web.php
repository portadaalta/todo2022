<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\AdminTasksController;
use App\Http\Controllers\AdminUsersController;
use App\Http\Controllers\EmailController;

use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard',[TasksController::class, 'index'])->name('dashboard');
    
    Route::get('/admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

    Route::get('/task',[TasksController::class, 'add']);
    Route::post('/task',[TasksController::class, 'create']);
    
    Route::get('/task/{task}', [TasksController::class, 'edit']);
    Route::post('/task/{task}', [TasksController::class, 'update']);    
});

    
Route::prefix('admin')->middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
    'is_admin'
])->group(function () {
	Route::get('/', function () {
    return view('admin.dashboard');
	});
	
	Route::get('/tasks',[AdminTasksController::class, 'index'])->name('tasksdashboard');    
    
    Route::get('/task',[AdminTasksController::class, 'add']);
    Route::post('/task',[AdminTasksController::class, 'create']);
    
    Route::get('/task/{task}', [AdminTasksController::class, 'edit']);
    Route::post('/task/{task}', [AdminTasksController::class, 'update']);
    
	Route::get('/users',[AdminUsersController::class, 'index'])->name('usersdashboard');  
	
});

Route::get('/sendemail', function () {
  try {
    $result = "Your email has been sent successfully";
    $data = array( 'name' => "LearningLaravel.net" );
    Mail::send('emails.learning', $data, function ($message) {
        $from = 'emisor@alumno.xyz';
        $name = 'nombreAlumno';
        // cambiar el email y poner uno propio
        $to = 'paco.portada@protonmail.com';
        $subject = "Learning Laravel test email";   
        $message->from($from, $name);
        $message->to($to);
        $message->subject($subject);
        });
    } catch (Exception $e) {
            $result = $e->getMessage();
    }
    return $result;
});

Route::get('/email', [EmailController::class, 'create']);
Route::post('/email', [EmailController::class, 'sendEmail'])->name('send.email');

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
 
    return redirect('/dashboard');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');


// Auth::routes(['verify' => true]);
